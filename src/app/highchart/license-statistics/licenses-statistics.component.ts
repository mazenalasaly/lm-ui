import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { LicensesStatistics } from 'src/app/_interface/licensesStatistics';

@Component({
  selector: 'app-licenses-statistics',
  templateUrl: './licenses-statistics.component.html',
  styleUrls: ['./licenses-statistics.component.css']
})
export class LicensesStatisticsComponent implements OnInit {
  private licensesStatistics!: LicensesStatistics;
  chartOptions: Highcharts.Options
  highcharts = Highcharts;
  constructor(private repoService: RepositoryService, private errorService: ErrorHandlerService) { }

  ngOnInit(): void {
    this.getLicensesStatistics();
  }


  setChartOptions() {
    this.chartOptions = {
      chart: {
        plotBackgroundColor: 'white',
        plotBorderWidth: 2,
        plotShadow: true,
        type: 'pie'
      },
      title: {
        text: 'Licenses Statistics'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          }
        }
      },
      series: [{
        name: 'Brands',
        colorByPoint: true,
        type: undefined,
        data: [{
          name: 'Activated licenses',
          y: this.licensesStatistics.activatedLicencesCounter,
          sliced: true,
          selected: true
        }, {
          name: 'Deactivated licenses',
          y: this.licensesStatistics.deactivatedLicencesCounter,
        },
        // {
        //   name: 'Licenses in need of renewal',
        //   y: this.licensesStatistics.licensesThatNeedsRenewalCounter,
        // },
        ]
      }]
    }
  }

  getLicensesStatistics() {
    this.repoService.getData('api/license/licenses/licensesStatistics')
      .subscribe(res => {
        this.licensesStatistics = res as LicensesStatistics;
        this.setChartOptions()
      },
        (error) => {
          this.errorService.handleError(error);
        })

  }

}
