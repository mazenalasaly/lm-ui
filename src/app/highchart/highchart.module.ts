import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighchartsChartModule } from 'highcharts-angular';
import { LicensesStatisticsComponent } from './license-statistics/licenses-statistics.component';

@NgModule({
  declarations: [LicensesStatisticsComponent],
  imports: [
    CommonModule, 
    HighchartsChartModule
  ],
  exports: [LicensesStatisticsComponent ]
})
export class HighchartModule { }
