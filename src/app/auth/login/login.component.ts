import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Router } from '@angular/router';
import { Credentials } from 'src/app/_interface/credentials';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  private dialogConfig;
 
  constructor(private router:Router, private repository: RepositoryService, private dialog: MatDialog, private errorService: ErrorHandlerService) { }
 
  ngOnInit() {
    this.loginForm = new FormGroup({
      userName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      userPassword: new FormControl('', [Validators.required, Validators.maxLength(60)]),
    });

    this.dialogConfig = {
      height: '220px',
      width: '440px',
      disableClose: true,
      data: { }
    }
  }
 
  public hasError (controlName: string, errorName: string){
    return this.loginForm.controls[controlName].hasError(errorName);
  }
  
  public login (loginFormValue) {
    if (this.loginForm.valid) {
      this.executeUserLogin(loginFormValue);
    }
  }
 
  private executeUserLogin (loginFormValue) {
      let credentials: Credentials = {
      userName: loginFormValue.userName,
      userPassword: loginFormValue.userPassword
    }
 
    let loginUrl = 'api/user/auth';
    this.repository.create(loginUrl, credentials)
      .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result=> {
            this.router.navigate(['home']);  
          });
      },
      (error => {
          this.errorService.errorDialogConfig = { ...this.dialogConfig };
          this.errorService.handleError(error);
      })
    )
  }

  public onCancel () {
    this.router.navigate(['home']);  
  }

  public redirectToSignup(){
    this.router.navigate(['auth', 'signup']);  
  }
  
}