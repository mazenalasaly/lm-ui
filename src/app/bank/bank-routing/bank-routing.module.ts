import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BankListComponent } from '../bank-list/bank-list.component';
import { BankCreateComponent } from '../bank-create/bank-create.component';
import { BankDeleteComponent } from '../bank-delete/bank-delete.component';
import { BankUpdateComponent } from '../bank-update/bank-update.component';
import { BankDetailsComponent } from '../bank-details/bank-details.component';
import { AddApplicationComponent } from '../add-application/add-application.component';
import { BankLicensesComponent } from '../bank-licenses/bank-licenses.component';


const routes: Routes = [
  { path: 'banks', component: BankListComponent },
  { path: 'details/:id', component: BankDetailsComponent },
  { path: ':id/addApplication', component: AddApplicationComponent },
  { path: 'create', component: BankCreateComponent },
  { path: 'update/:id', component: BankUpdateComponent },
  { path: 'delete/:id', component: BankDeleteComponent },
  { path: 'listLicenses/:id', component: BankLicensesComponent },
  
];
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class BankRoutingModule { }
