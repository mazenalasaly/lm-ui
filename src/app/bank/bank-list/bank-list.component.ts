import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator'
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Router } from '@angular/router';
import { Bank } from 'src/app/_interface/bank';
import { ConfirmDialogComponent } from 'src/app/shared/dialogs/confirm-dialog/confirm-dialog.component';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material/dialog';
 
@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.css']
})
export class BankListComponent implements OnInit, AfterViewInit {
 
  public displayedColumns = ['bankName', 'bankCode', 'listLicenses', 'update', 'delete'];
  public dataSource = new MatTableDataSource<Bank>();
  private confirmationDialogConfig;
  private successDialogConfig;
  private errorMessage: string = '';

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
 
  constructor(private repoService: RepositoryService, private errorService: ErrorHandlerService, private router: Router, private dialog: MatDialog) { }
 
  ngOnInit() {
    this.getAllBanks();
    this.confirmationDialogConfig = {
      data: {
        title: 'Confirm Delete Bank',
        message: 'You are about to delete a bank, Are you sure? '
      }
    }
    this.successDialogConfig = {
      data: {
        title: 'Deleted Successfully',
        message: 'The selected bank has been deleted successfully'
      },
    }
  }
 
  public getAllBanks()  {
    this.repoService.getData('api/bank/banks')
    .subscribe(res=> {
      this.dataSource.data = res as Bank[];
    },
    (error)=> {
      this.errorService.handleError(error);
    })
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public customSort (event)  {
    console.log(event);
  }

  public doFilter (value: string)  {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // public redirectToAddApplication (id: Number)  {
  //   this.router.navigate(['bank', id, 'addApplication']); 
  // }
 
  public redirectToDetails (id: Number)  {
    this.router.navigate(['bank','details',id]);    
  }
 
  public redirectToUpdate (id: Number)  {
    this.router.navigate(['bank','update',id]);       
  }
 
  public redirectToDelete (id: Number)  {
    this.router.navigate(['bank','delete',id]);       
  }
  public redirectTolistLicenses (id: Number)  {
    this.router.navigate(['bank','listLicenses',id]);       
  }

  deleteBank(id: Number) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, this.confirmationDialogConfig);
    
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
       this.executeDeleteBank(id);
      }
    });
  }

  public executeDeleteBank (id: Number) {
    const deleteUrl: string = `api/bank/delete/${id}`;
   
    this.repoService.delete(deleteUrl)
    .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
        dialogRef.afterClosed()
           .subscribe(result=> {
            this.refreshPage();
          });
    },
      (error)=> {
        this.errorService.handleError(error);
        this.errorMessage = this.errorService.errorMessage;
      })
  }

  refreshPage() {
    window.location.reload();
   }

 
}
