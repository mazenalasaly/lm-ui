import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankListComponent } from './bank-list/bank-list.component';
import { BankRoutingModule } from './bank-routing/bank-routing.module';
import { BankCreateComponent } from './bank-create/bank-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BankDeleteComponent } from './bank-delete/bank-delete.component';
import { BankUpdateComponent } from './bank-update/bank-update.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { BankDataComponent } from './bank-details/bank-data/bank-data.component';
import { BankLicenseDataComponent } from './bank-details/bank-license-data/bank-license-data.component';
import { AddApplicationComponent } from './add-application/add-application.component';
import { BankLicensesComponent } from './bank-licenses/bank-licenses.component';




@NgModule({
  declarations: [BankListComponent, BankDetailsComponent, BankDataComponent, BankLicenseDataComponent, BankCreateComponent, BankDeleteComponent, BankUpdateComponent, AddApplicationComponent, BankLicensesComponent],
  imports: [
    CommonModule,
    BankRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [BankListComponent, BankDetailsComponent, BankDataComponent, BankLicenseDataComponent, BankCreateComponent, BankDeleteComponent, BankUpdateComponent, BankLicensesComponent],
})
export class BankModule { }
