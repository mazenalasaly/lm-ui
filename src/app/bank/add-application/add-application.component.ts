import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { Bank } from 'src/app/_interface/bank';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Application } from 'src/app/_interface/application';
import { ConfirmDialogComponent } from 'src/app/shared/dialogs/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from 'src/app/shared/dialogs/error-dialog/error-dialog.component';

 
@Component({
  selector: 'app-add-application',
  templateUrl: './add-application.component.html',
  styleUrls: ['./add-application.component.css']
})

export class AddApplicationComponent implements OnInit {
 
  public errorMessage: string = '';
  public bank: Bank;
  public applications: Application[];
  public licenseCodes: string[];
  public addApplicationForm: FormGroup;
  private successDialogConfig;
  private errorDialogConfig;
  private confirmationDialogConfig;
  private licenseConfirmationDialogConfig;
  
 
  constructor(private repository: RepositoryService, private errorService: ErrorHandlerService, private router: Router,
    private activeRoute: ActivatedRoute, private dialog: MatDialog) { }
 
    ngOnInit() {
      this.getAllLicenseCodes();
      this.getBankById();
      this.getAllApplications();
      this.addApplicationForm = new FormGroup({
        application: new FormControl('', [Validators.required] )
      });

      this.successDialogConfig = {
        data: {
          title: "Updated Successfully",
          message: "The required bank has been successfully updated"
        }
      }

      this.confirmationDialogConfig = {
        data: {
          title: "Are you sure?",
          message: "You are about to add application already exist in this bank"
        }
      }
      this.licenseConfirmationDialogConfig = {
        data: {
          title: "Are you sure?",
          message: "You are about to add that does not have a license"
        }
      }

      this.errorDialogConfig = {
        data: {
          title: "Error Message",
          message: "You are about to add application already exist in this bank"
        }
      }

    }

    ngAfterViewInit() {
    
    }
     

    public validateControl(controlName: string) {
      if (this.addApplicationForm.controls[controlName].invalid && this.addApplicationForm.controls[controlName].touched)
        return true;
     
      return false;
    }
     
    public hasError(controlName: string, errorName: string) {
      if (this.addApplicationForm.controls[controlName].hasError(errorName))
        return true;
     
      return false;
    }
          
    public redirectToBankList() {
      this.router.navigate(['bank','banks']);    
    }

    public addApplication(addApplicationFormValue) {
      if (this.addApplicationForm.valid) {
        let selectedApplication: Application;
        this.applications.forEach(applicationFilteredFromList => {
          if (applicationFilteredFromList.id == addApplicationFormValue.application) {
            selectedApplication = applicationFilteredFromList;
          }
        })
        
        this.checkIfTheApplicationAlreadyExistsInThisBank(this.bank.bankName, selectedApplication.applicationName, addApplicationFormValue);
      }
    }
     
    private executeAddApplication(addApplicationFormValue) {
       let applicationId: Number =addApplicationFormValue.application;
      const apiUrl = `api/bank/${this.bank.id}/addApplication/${applicationId}`;
      this.repository.update(apiUrl, this.bank)
        .subscribe(res=> {
          let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
          dialogRef.afterClosed()
            .subscribe(result=> {
              this.router.navigate(['bank','banks']); 
            });
        },
        (error=> {
          this.errorService.errorDialogConfig = { ...this.errorDialogConfig };
          this.errorService.handleError(error);
          // this.errorMessage = this.errorService.errorMessage;
        })
      )
    }

    

    private getBankById() {
      let bankId: string = this.activeRoute.snapshot.params['id'];       
      let bankByIdUrl: string = `api/bank/details/${bankId}`;
      this.repository.getData(bankByIdUrl)
        .subscribe(res=> {
          this.bank = res as Bank;
        },
        (error)=> {
          this.errorService.handleError(error);
          this.errorMessage = this.errorService.errorMessage;
        })
    }


  public getAllApplications() {
    this.repository.getData('api/application/applications')
      .subscribe(res => {
        this.applications = res as Application[];
      },
        (error) => {
          this.errorService.handleError(error);
        })
  } 

  public getAllLicenseCodes() {
    this.repository.getData('api/license/licenseCodes')
      .subscribe(res => {
        this.licenseCodes = res as string[];
      },
        (error) => {
          this.errorService.handleError(error);
        })
        
  } 
 
  public checkIfTheApplicationAlreadyExistsInThisBank (bankName: string, applicationName:string, addApplicationFormValue){
    let localLicenseCode = bankName.concat(applicationName);
    for (let licenseCode of this.licenseCodes) {
      if( localLicenseCode == licenseCode){
        let dialogRef = this.dialog.open(ConfirmDialogComponent, this.confirmationDialogConfig);
        dialogRef.afterClosed()
            .subscribe(result=> {
              this.router.navigate(['bank','banks']); 
            })

      }
      else {
        this.executeAddApplication(addApplicationFormValue);
      }
    }
  }

  
}


 

 

     
