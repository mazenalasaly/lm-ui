import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { Bank } from 'src/app/_interface/bank';
import { SuccessDialogComponent } from '../../shared/dialogs/success-dialog/success-dialog.component';
 
@Component({
  selector: 'app-bank-delete',
  templateUrl: './bank-delete.component.html',
  styleUrls: ['./bank-delete.component.css']
})
export class BankDeleteComponent implements OnInit {
  public errorMessage: string = '';
  public bank: Bank;
  private dialogSuccessConfig;
 
constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
  private activeRoute: ActivatedRoute, private dialog: MatDialog) { }
 
  ngOnInit() {
    this.getBankById();
    
    this.dialogSuccessConfig = {
      height: '220px',
      width: '440px',
      disableClose: true,
      data: { 
        title: 'Deleted Succesfully',        
        message: 'The selected Bank is deleted successfully'
    }
  }
}
   
  private getBankById () {
    const bankId: string = this.activeRoute.snapshot.params['id'];
    const bankByIdUrl: string = `api/bank/details/${bankId}`;
   
    this.repository.getData(bankByIdUrl)
      .subscribe(res=> {
        this.bank = res as Bank;
      },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }
   
     
  public redirectToBankList () {
    this.router.navigate(['bank','banks']);    
  }

  public deleteBank () {
    const deleteUrl: string = `api/bank/delete/${this.bank.id}`;
   
    this.repository.delete(deleteUrl)
    .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.dialogSuccessConfig);
        dialogRef.afterClosed()
           .subscribe(result=> {
            this.redirectToBankList();
          });
    },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }
}

