import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { BankForCreation } from 'src/app/_interface/bankForCreation';
import { Bank } from 'src/app/_interface/bank';
import { Router } from '@angular/router';
import { Application } from 'src/app/_interface/application';

@Component({
  selector: 'app-bank-create',
  templateUrl: './bank-create.component.html',
  styleUrls: ['./bank-create.component.css']
})
export class BankCreateComponent implements OnInit {
  public bankForm: FormGroup;
  private successDialogConfig;
  private errorDialogConfig;  
  applications: Application[];

  constructor(private router: Router, private repository: RepositoryService, private dialog: MatDialog, private errorService: ErrorHandlerService) { }

  ngOnInit() {
    this.getAllApplications();
    this.bankForm = new FormGroup({
     bankName: new FormControl('', [Validators.required]),
     bankCode: new FormControl('', [Validators.required]),
     application: new FormControl('',)
    });

    this.successDialogConfig = {
      data: {
        title: "Created Successfully",
        message: "The required bank has been successfully created"
      }
    }

    this.errorDialogConfig = {
      data: {
        title: "Error Message",
        message: " "
      }
    }

  }

  public hasError(controlName: string, errorName: string) {
    return this.bankForm.controls[controlName].hasError(errorName);
  }

  public onCancel() {
    this.router.navigate(['bank', 'banks']);
  }

  public createBank(bankFormValue) {
    if (this.bankForm.valid) {
      this.executeBankCreation(bankFormValue);
    }
  }

  private executeBankCreation(bankFormValue) {
    // let selectedApplication: Application;
    // this.applications.forEach(applicationFilteredFromList => {
    //   if (applicationFilteredFromList.id == bankFormValue.application) {
    //     selectedApplication = applicationFilteredFromList;
    //   }
    // })
    
    let bank: BankForCreation = {
      bankName: bankFormValue.bankName,
      bankCode: bankFormValue.bankCode,
      // applicationId: bankFormValue.application
    }

    let apiUrl = 'api/bank/create';
    this.repository.create(apiUrl, bank)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
        dialogRef.afterClosed()
          .subscribe(result => {
            this.router.navigate(['bank', 'banks']);
          });
      },
        (error => {
          this.errorService.errorDialogConfig = { ...this.errorDialogConfig };
          this.errorService.handleError(error);
        })
      )
  }


  public getAllApplications() {
    this.repository.getData('api/application/applications')
      .subscribe(res => {
        this.applications = res as Application[];
      },
        (error) => {
          this.errorService.handleError(error);
        })
  }  

}

