import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Bank } from 'src/app/_interface/bank';
 
@Component({
  selector: 'app-bank-data',
  templateUrl: './bank-data.component.html',
  styleUrls: ['./bank-data.component.css']
})
export class BankDataComponent implements OnInit {
  @Input() public bank: Bank;
  public selectOptions = [{name:'Show', value: 'show'}, {name: `Don't Show`, value: ''}];
  @Output() selectEmitt = new EventEmitter();
 
  constructor() { }
 
  ngOnInit() {
  }
 
  public onChange(event) {
    this.selectEmitt.emit(event.value);
  }
 
}
