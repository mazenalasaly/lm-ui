import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RepositoryService } from '../../shared/repository.service';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Bank } from 'src/app/_interface/bank';
import { License } from 'src/app/_interface/license';
 
@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class BankDetailsComponent implements OnInit {
  public bank: Bank;
  public showLicenses;
  private licenses: License[];
 
  constructor(private repository: RepositoryService, private router: Router, 
    private activeRoute: ActivatedRoute, private errorHandler: ErrorHandlerService) { }
 
  ngOnInit() {
    this.getBankDetails();
  }
 
  private getBankDetails(){
    let id: string = this.activeRoute.snapshot.params['id'];
    let apiUrl: string = `api/bank/${id}/licenses`;
 
    this.repository.getData(apiUrl)
    .subscribe(res=> {
      this.bank = res as Bank;
    },
    (error)=>{
      this.errorHandler.handleError(error);
    })
  }
}