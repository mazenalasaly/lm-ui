import { Component, OnInit, Input } from '@angular/core';
import { License } from 'src/app/_interface/license';

 
@Component({
  selector: 'app-bank-license-data',
  templateUrl: './bank-license-data.component.html',
  styleUrls: ['./bank-license-data.component.css']
})
export class BankLicenseDataComponent implements OnInit {
  @Input() public licenses: License[];
  constructor() { }
 
  ngOnInit() {
  }
 
}