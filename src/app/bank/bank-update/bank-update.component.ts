import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { Bank } from 'src/app/_interface/bank';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material/dialog';

 
@Component({
  selector: 'app-bank-update',
  templateUrl: './bank-update.component.html',
  styleUrls: ['./bank-update.component.css']
})
export class BankUpdateComponent implements OnInit {
 
  public errorMessage: string = '';
  public bank: Bank;
  public bankForm: FormGroup;
  private   successDialogConfig;
 
  constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
    private activeRoute: ActivatedRoute, private dialog: MatDialog) { }
 
    ngOnInit() {
      this.bankForm = new FormGroup({
        bankName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
        bankCode: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      });

      this.successDialogConfig = {
        data: {
          title: "Updated Successfully",
          message: "The required bank has been successfully updated"
        }
      }
     
      this.getBankById();
    }
     
    ngAfterViewInit() {
     
    }

    private getBankById() {
      let bankId: string = this.activeRoute.snapshot.params['id'];
        
      let bankByIdUrl: string = `api/bank/details/${bankId}`;
     
      this.repository.getData(bankByIdUrl)
        .subscribe(res=> {
          this.bank = res as Bank;
          console.log(this.bank);
          this.bankForm.patchValue(this.bank);
        },
        (error)=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
    }

    public validateControl(controlName: string) {
      if (this.bankForm.controls[controlName].invalid && this.bankForm.controls[controlName].touched)
        return true;
     
      return false;
    }
     
    public hasError(controlName: string, errorName: string) {
      if (this.bankForm.controls[controlName].hasError(errorName))
        return true;
     
      return false;
    }
          
    public redirectToBankList() {
      this.router.navigate(['bank','banks']);    
    }

    public updateBank(bankFormValue) {
      if (this.bankForm.valid) {
        this.executeBankUpdate(bankFormValue);
      }
    }
     
    private executeBankUpdate(bankFormValue) {
      this.bank.bankName = bankFormValue.bankName;
      this.bank.bankCode = bankFormValue.bankCode;
     
      const apiUrl = `api/bank/update/${this.bank.id}`;
      this.repository.update(apiUrl, this.bank)
        .subscribe(rres=> {
          let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
   
          //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
          dialogRef.afterClosed()
            .subscribe(result=> {
              this.router.navigate(['bank','banks']); 
            });
        },
        (error=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
      )
    }
  
}
