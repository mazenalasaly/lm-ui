import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { NotFoundComponent } from '../error-pages/not-found/not-found.component';
import { ServerErrorComponent } from '../error-pages/server-error/server-error.component';


const routes: Routes = [
  { path:'auth', loadChildren: () => import ('./../auth/auth.module').then(m => m.AuthModule)},
  { path: 'home', component: HomeComponent},
  { path: 'user', loadChildren: () => import ('./../user/user.module').then(m => m.UserModule) },
  { path: 'auth', loadChildren: () => import ('./../auth/auth.module').then(m => m.AuthModule) },
  { path: 'license', loadChildren: () => import ('./../license/license.module').then(m => m.LicenseModule) }, 
  { path: 'bank', loadChildren: () => import ('./../bank/bank.module').then(m => m.BankModule) }, 
  { path: 'application', loadChildren: () => import ('./../application/application.module').then(m => m.ApplicationModule) }, 
  { path: '404', component: NotFoundComponent},
  { path: '500', component: ServerErrorComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/404', pathMatch: 'full'}
 
];
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
