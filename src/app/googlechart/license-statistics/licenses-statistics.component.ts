import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { LicensesStatistics } from 'src/app/_interface/licensesStatistics';

@Component({
  selector: 'app-licenses-statistics',
  templateUrl: './licenses-statistics.component.html',
  styleUrls: ['./licenses-statistics.component.css']
})
export class LicensesStatisticsComponent implements OnInit {
  private licensesStatistics!: LicensesStatistics;
  
  constructor(private repoService: RepositoryService, private errorService: ErrorHandlerService) { }

  ngOnInit(): void {
    this.getLicensesStatistics();
  }

  title = 'Licenses Statistics';
  type = 'PieChart';
  data = [
     ['Firefox', 45.0],
     ['IE', 26.8],
     ['Chrome', 12.8],
     ['Safari', 8.5],
     ['Opera', 6.2],
     ['Others', 0.7] 
  ];
  columnNames = ['License', 'Percentage'];
  options = {          
  };
  width = 400;
  height = 400;


   getLicensesStatistics() {
    this.repoService.getData('api/license/licenses/licensesStatistics')
      .subscribe(res => {
        this.licensesStatistics = res as LicensesStatistics;
      },
        (error) => {
          this.errorService.handleError(error);
        })

  }

}
