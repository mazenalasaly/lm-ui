import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleChartsModule } from 'angular-google-charts';
import { LicensesPerBankComponent } from './licenses-per-bank/licenses-per-bank.component';


@NgModule({
  // declarations: [LicensesStatisticsComponent, LicensesPerBankComponent, BarChartComponent],
  declarations: [LicensesPerBankComponent],
  imports: [
    CommonModule,
    GoogleChartsModule 
  ], 
  // exports: [LicensesStatisticsComponent, LicensesPerBankComponent, BarChartComponent ]
  exports: [LicensesPerBankComponent ]
})
export class GooglechartModule { }
