import { Component, OnInit } from '@angular/core';
import { GoogleChartComponent } from 'angular-google-charts';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';

@Component({
  selector: 'app-licenses-per-bank',
  templateUrl: './licenses-per-bank.component.html',
  styleUrls: ['./licenses-per-bank.component.css']
})
export class LicensesPerBankComponent implements OnInit {
  private licensesPerBank: any;
  chartData: any;
  title: any;

  constructor(private repoService: RepositoryService, private errorService: ErrorHandlerService) { }

  ngOnInit(): void {
    this.getlicensesPerBank();
  }

  setChart() {
    this.title= 'Lisenses Per Bank';
    this.chartData = {
      type: 'ColumnChart',
      data: this.licensesPerBank,
      chartColumns: ['Bank', 'Licenses'],
      width: 600,
      height: 400
    };
  }

  getlicensesPerBank() {
    this.repoService.getData('api/license/licenses/licensesPerBank')
      .subscribe(res => {
        this.licensesPerBank = res;
        this.setChart();
      },
        (error) => {
          this.errorService.handleError(error);
        })

  }

}
