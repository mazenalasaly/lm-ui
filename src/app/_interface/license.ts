import { Application } from "./application";
import { Bank } from "./bank";

export interface License {
    id: Number;
    bank: Bank;
    application: Application;
    licenseCode: string;
    dateCreated: Date;
    createdBy: Number;
    isActivated: boolean;
    dateExpired: Date;
    licenseFile: Blob;

}
