import { Application } from "./application";
import { License } from "./license";

export interface Bank {
    id: Number;
    bankName: string;
    bankCode:string;
    licenses: License[];   
}
