export interface User {

    id: Number;
    userName: string;
    userPassword: string;
    userEmail: string;
    dateOfBirth: Date;
    isActivated: boolean;
}
