import { Bank } from "./bank";

export interface ApplicationForCreation {
    applicationName: string;
    applicationDetails: string;
}
