export interface LicensesStatistics {
    activatedLicencesCounter: Number;
    deactivatedLicencesCounter: Number;
    licensesThatNeedsRenewalCounter: Number;
}

  
  