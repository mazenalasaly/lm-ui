import { License } from "./license";

export interface Application {
    id: Number;
    applicationName: string;
    applicationDetails: string;
    licenses: License[];
}
