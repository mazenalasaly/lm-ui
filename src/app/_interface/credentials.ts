export interface Credentials {
    userName: string;
    userPassword: string;
}

export interface LicenseRelationships {
    bankId: Number;
    applicationId: Number;
}
 
