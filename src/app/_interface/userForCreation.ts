export interface UserForCreation {
    userName: string;
    userPassword: string;
    userEmail: string;
    dateOfBirth: Date;
    isActivated: boolean;
}
