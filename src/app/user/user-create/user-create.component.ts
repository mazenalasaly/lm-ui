import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { UserForCreation } from 'src/app/_interface/userForCreation';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  public userForm: FormGroup;
  private dialogConfig;
 
  constructor(private router:Router, private repository: RepositoryService, private dialog: MatDialog, private errorService: ErrorHandlerService) { }
 
  ngOnInit() {
    this.userForm = new FormGroup({
      userName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      userPassword: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      userEmail: new FormControl('', [Validators.required, Validators.maxLength(60), Validators.email]),
      dateOfBirth: new FormControl(new Date(), [Validators.required]),
    });

    this.dialogConfig = {
      data: { }
    }
  }
 
  public hasError (controlName: string, errorName: string){
    return this.userForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel () {
    this.router.navigate(['user','users']);  
  }
 
  public createUser (userFormValue) {
    if (this.userForm.valid) {
      this.executeUserCreation(userFormValue);
    }
  }
 
  private executeUserCreation (userFormValue) {
      let user: UserForCreation = {
      userName: userFormValue.userName,
      userPassword: userFormValue.userPassword,
      userEmail: userFormValue.userEmail,
      dateOfBirth: userFormValue.dateOfBirth,
      isActivated: true
    }
 
    let apiUrl = 'api/user/create';
    this.repository.create(apiUrl, user)
      .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.dialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result=> {
            this.router.navigate(['user','users']);  
          });
      },
      (error => {
          this.errorService.errorDialogConfig  = { ...this.dialogConfig };
          this.errorService.handleError(error);
      })
    )
  }
  
}