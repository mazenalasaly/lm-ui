import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator'
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Router } from '@angular/router';
import { User } from 'src/app/_interface/user';
import { MatDialog } from '@angular/material/dialog';
 
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, AfterViewInit {
 
  public displayedColumns = ['userName', 'userPassword', 'userEmail', 'dateOfBirth', 'update', 'delete'];
  public dataSource = new MatTableDataSource<User>();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
 
  constructor(private repoService: RepositoryService, private errorService: ErrorHandlerService, private router: Router, private dialog: MatDialog) { }
 
  ngOnInit() {
    this.getAllUsers();
  }
 
  public getAllUsers() {
    this.repoService.getData('api/user/users')
    .subscribe(res=> {
      this.dataSource.data = res as User[];
    },
    (error)=> {
      this.errorService.handleError(error);
    })
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public customSort (event)  {
    console.log(event);
  }

  public doFilter (value: string)  {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
  
  public redirectToUpdate (id: number)  {
    this.router.navigate(['user','update',id]);     
  }
 
  public redirectToDelete (id: number)  {
    this.router.navigate(['user','delete',id]);    
  }
 
}
