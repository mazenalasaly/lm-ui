import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { User } from 'src/app/_interface/user';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material/dialog';

 
@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {
 
  public errorMessage: string = '';
  public user: User;
  public userForm: FormGroup;
  private   dialogConfig;
 
  constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
    private activeRoute: ActivatedRoute, private datePipe: DatePipe,  private dialog: MatDialog) { }
 
    ngOnInit() {
      this.userForm = new FormGroup({
        userName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
        userPassword: new FormControl('', [Validators.required, Validators.maxLength(60)]),
        userEmail: new FormControl('', [Validators.required, Validators.maxLength(60), Validators.email]),
        dateOfBirth: new FormControl('', [Validators.required]),
      });

      this.dialogConfig = {
        data: { }
      }
     
      this.getUserById();
    }
     
    ngAfterViewInit() {
     
    }

    private getUserById() {
      let userId: string = this.activeRoute.snapshot.params['id'];
        
      let userByIdUrl: string = `api/user/details/${userId}`;
     
      this.repository.getData(userByIdUrl)
        .subscribe(res=> {
          this.user = res as User;
          console.log(this.user);
          this.userForm.patchValue(this.user);
        },
        (error)=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
    }

    public validateControl(controlName: string) {
      if (this.userForm.controls[controlName].invalid && this.userForm.controls[controlName].touched)
        return true;
     
      return false;
    }
     
    public hasError(controlName: string, errorName: string) {
      if (this.userForm.controls[controlName].hasError(errorName))
        return true;
     
      return false;
    }
     
    public executeDateOfBirthPicker(event) {
      this.userForm.patchValue({ 'dateOfBirth': event });
    }

    public redirectToUserList() {
      this.router.navigate(['user','users']);    
    }

    public updateUser(userFormValue) {
      if (this.userForm.valid) {
        this.executeUserUpdate(userFormValue);
      }
    }
     
    private executeUserUpdate(userFormValue) {
      this.user.userName = userFormValue.userName;
      this.user.userPassword = userFormValue.userPassword;
      this.user.userEmail= userFormValue.userEmail;
      this.user.dateOfBirth = userFormValue.dateOfBirth;
     
      const apiUrl = `api/user/update/${this.user.id}`;
      this.repository.update(apiUrl, this.user)
        .subscribe(rres=> {
          let dialogRef = this.dialog.open(SuccessDialogComponent, this.dialogConfig);
   
          //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
          dialogRef.afterClosed()
            .subscribe(result=> {
              this.router.navigate(['user','users']); 
            });
        },
        (error=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
      )
    }
  
}
