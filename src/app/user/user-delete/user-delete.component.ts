import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { User } from 'src/app/_interface/user';
import { SuccessDialogComponent } from '../../shared/dialogs/success-dialog/success-dialog.component';
 
@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css']
})
export class UserDeleteComponent implements OnInit {
  public errorMessage: string = '';
  public user: User;
  private dialogSuccessConfig;
 
constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
  private activeRoute: ActivatedRoute, private dialog: MatDialog) { }
 
  ngOnInit() {
    this.getUserById();
    
    this.dialogSuccessConfig = {
      height: '220px',
      width: '440px',
      disableClose: true,
      data: { message: 'It is deleted successfully',
      buttonText: {
        cancel: 'Done' }
    },
  }
}
   
  private getUserById () {
    const userId: string = this.activeRoute.snapshot.params['id'];
    const userByIdUrl: string = `api/user/details/${userId}`;
   
    this.repository.getData(userByIdUrl)
      .subscribe(res=> {
        this.user = res as User;
      },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }
   
     
  public redirectToUserList () {
    this.router.navigate(['user','users']);    
  }

  public deleteUser () {
    const deleteUrl: string = `api/user/delete/${this.user.id}`;
   
    this.repository.delete(deleteUrl)
    .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.dialogSuccessConfig);
        dialogRef.afterClosed()
           .subscribe(result=> {
            this.redirectToUserList();
          });
    },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }
}

