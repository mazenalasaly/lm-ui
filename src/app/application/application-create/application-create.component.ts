import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Router } from '@angular/router';
import { ApplicationForCreation } from 'src/app/_interface/applicationForCreation';
 
@Component({
  selector: 'app-application-create',
  templateUrl: './application-create.component.html',
  styleUrls: ['./application-create.component.css']
})
export class ApplicationCreateComponent implements OnInit {
  public applicationForm: FormGroup;
  private successDialogConfig;
  private errorDialogConfig;
 
  constructor(private location:Location, private router: Router, private repository: RepositoryService, private dialog: MatDialog, private errorService: ErrorHandlerService) { }
 
  ngOnInit() {
    this.applicationForm = new FormGroup({
      applicationName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      applicationDetails: new FormControl('', [Validators.required, Validators.maxLength(100)])
    });

    this.successDialogConfig = {
      data: {
        title: "Created Successfully",
        message: "The required application has been successfully created"
      }
    }

    this.errorDialogConfig = {
      data: {
        title: "Error Message",
        message: " "
      }
    }
  }
 
  public hasError(controlName: string, errorName: string){
    return this.applicationForm.controls[controlName].hasError(errorName);
  }
 
  public onCancel() {
    let detailsUrl: string = `/application/applications/`;
    this.router.navigate(['application','applications']);    
  }
 
  public createApplication (applicationFormValue) {
    if (this.applicationForm.valid) {
      this.executeApplicationCreation(applicationFormValue);
    }
  }
 
  private executeApplicationCreation (applicationFormValue)  {
    let application: ApplicationForCreation = {
      applicationName: applicationFormValue.applicationName,
      applicationDetails: applicationFormValue.applicationDetails
    }
    let apiUrl = 'api/application/create';
    this.repository.create(apiUrl, application)
      .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent,this.successDialogConfig);
 
        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed()
          .subscribe(result=> {
            this.location.back();
          });
      },
      (error)=> {
          this.errorService.errorDialogConfig = { ...this.errorDialogConfig };
          this.errorService.handleError(error);
      })
  }
 
}