import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationListComponent } from '../application-list/application-list.component';
import { ApplicationCreateComponent } from '../application-create/application-create.component';
import { ApplicationDeleteComponent } from '../application-delete/application-delete.component';
import { ApplicationUpdateComponent } from '../application-update/application-update.component';import { ApplicationDetailsComponent } from '../application-details/application-details.component';
import { ApplicationLicensesComponent } from '../application-licenses/application-licenses.component';
;


const routes: Routes = [
  { path: 'applications', component: ApplicationListComponent },
  { path: 'details/:id', component: ApplicationDetailsComponent },
  { path: 'create', component: ApplicationCreateComponent },
  { path: 'update/:id', component: ApplicationUpdateComponent },
  { path: 'delete/:id', component: ApplicationDeleteComponent },
  { path: 'listLicenses/:id', component: ApplicationLicensesComponent },
];
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class ApplicationRoutingModule { }
