import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { Application } from 'src/app/_interface/application';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material/dialog';

 
@Component({
  selector: 'app-application-update',
  templateUrl: './application-update.component.html',
  styleUrls: ['./application-update.component.css']
})
export class ApplicationUpdateComponent implements OnInit {
 
  public errorMessage: string = '';
  public application: Application;
  public applicationForm: FormGroup;
  private   successDialogConfig;
 
  constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
    private activeRoute: ActivatedRoute, private dialog: MatDialog) { }
 
    ngOnInit() {
      this.applicationForm = new FormGroup({
        applicationName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
        applicationDetails: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      });

      this.successDialogConfig = {
        data: {
          title: "Updated Successfully",
          message: "The required application has been successfully updated"
        }
      }
     
      this.getApplicationById();
    }
     
    ngAfterViewInit() {
     
    }

    private getApplicationById() {
      let applicationId: string = this.activeRoute.snapshot.params['id'];
        
      let applicationByIdUrl: string = `api/application/details/${applicationId}`;
     
      this.repository.getData(applicationByIdUrl)
        .subscribe(res=> {
          this.application = res as Application;
          console.log(this.application);
          this.applicationForm.patchValue(this.application);
        },
        (error)=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
    }

    public validateControl(controlName: string) {
      if (this.applicationForm.controls[controlName].invalid && this.applicationForm.controls[controlName].touched)
        return true;
     
      return false;
    }
     
    public hasError(controlName: string, errorName: string) {
      if (this.applicationForm.controls[controlName].hasError(errorName))
        return true;
     
      return false;
    }
          
    public redirectToApplicationList() {
      this.router.navigate(['application','applications']);    
    }

    public updateApplication(applicationFormValue) {
      if (this.applicationForm.valid) {
        this.executeApplicationUpdate(applicationFormValue);
      }
    }
     
    private executeApplicationUpdate(applicationFormValue) {
      this.application.applicationName = applicationFormValue.applicationName;
      this.application.applicationDetails = applicationFormValue.applicationDetails;
     
      const apiUrl = `api/application/update/${this.application.id}`;
      this.repository.update(apiUrl, this.application)
        .subscribe(rres=> {
          let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
   
          //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
          dialogRef.afterClosed()
            .subscribe(result=> {
              this.router.navigate(['application','applications']); 
            });
        },
        (error=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
      )
    }
  
}
