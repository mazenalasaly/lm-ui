import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator'
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Router } from '@angular/router';
import { Application } from 'src/app/_interface/application';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { ConfirmDialogComponent } from 'src/app/shared/dialogs/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css']
})
export class ApplicationListComponent implements OnInit, AfterViewInit {

  public displayedColumns = ['applicationName', 'applicationDetails', 'listLicenses', 'update', 'delete'];
  public dataSource = new MatTableDataSource<Application>();
  private confirmationDialogConfig;
  private successDialogConfig;
  private errorMessage: string = " ";
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private repoService: RepositoryService, private errorService: ErrorHandlerService, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.getAllApplications();
    this.confirmationDialogConfig = {
      data: {
        title: 'Confirm Delete Application',
        message: 'You are about to delete an application, Are you sure? '
      }
    }
    this.successDialogConfig = {
      data: {
        title: 'Deleted Successfully',
        message: 'The selected application has been deleted successfully'
      },
    }
  }

  public getAllApplications() {
    this.repoService.getData('api/application/applications')
      .subscribe(res => {
        this.dataSource.data = res as Application[];
      },
        (error) => {
          this.errorService.handleError(error);
        })
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public customSort(event) {
    console.log(event);
  }

  public doFilter(value: string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  public redirectToDetails(id: Number) {
    this.router.navigate(['application', 'details', id]);
  }

  public redirectToUpdate(id: Number) {
    this.router.navigate(['application', 'update', id]);
  }

  public redirectToDelete(id: Number) {
    this.router.navigate(['application', 'delete', id]);
  }
  
  public redirectTolistLicenses(id: Number) {
    this.router.navigate(['application', 'listLicenses', id]);
  }

  deleteApplication(id: Number) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, this.confirmationDialogConfig);

    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.executeDeleteApplication(id);
      }
    });
  }

  public executeDeleteApplication(id: Number) {
    const deleteUrl: string = `api/application/delete/${id}`;

    this.repoService.delete(deleteUrl)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
        dialogRef.afterClosed()
          .subscribe(result => {
            this.refreshPage();
          });
      },
        (error) => {
          this.errorService.handleError(error);
          this.errorMessage = this.errorService.errorMessage;
        })
  }

  refreshPage() {
    window.location.reload();
  }

}
