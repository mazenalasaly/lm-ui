import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { Application } from 'src/app/_interface/application';
import { SuccessDialogComponent } from '../../shared/dialogs/success-dialog/success-dialog.component';
 
@Component({
  selector: 'app-application-delete',
  templateUrl: './application-delete.component.html',
  styleUrls: ['./application-delete.component.css']
})
export class ApplicationDeleteComponent implements OnInit {
  public errorMessage: string = '';
  public application: Application;
  private dialogSuccessConfig;
 
constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
  private activeRoute: ActivatedRoute, private dialog: MatDialog) { }
 
  ngOnInit() {
    this.getApplicationById();
    
    this.dialogSuccessConfig = {
      height: '220px',
      width: '440px',
      disableClose: true,
      data: { 
        title: 'Deleted Successfully',
        message: 'The required application has been successfully deleted',
    }
  }
}
   
  private getApplicationById () {
    const applicationId: string = this.activeRoute.snapshot.params['id'];
    const applicationByIdUrl: string = `api/application/details/${applicationId}`;
   
    this.repository.getData(applicationByIdUrl)
      .subscribe(res=> {
        this.application = res as Application;
      },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }
   
     
  public redirectToApplicationList () {
    this.router.navigate(['application','applications']);    
  }

  public deleteApplication () {
    const deleteUrl: string = `api/application/delete/${this.application.id}`;
   
    this.repository.delete(deleteUrl)
    .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.dialogSuccessConfig);
        dialogRef.afterClosed()
           .subscribe(result=> {
            this.redirectToApplicationList();
          });
    },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }
}

