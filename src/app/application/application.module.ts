import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationRoutingModule } from './application-routing/application-routing.module';
import { ApplicationCreateComponent } from './application-create/application-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ApplicationDeleteComponent } from './application-delete/application-delete.component';
import { ApplicationUpdateComponent } from './application-update/application-update.component';
import { ApplicationDataComponent } from './application-details/application-data/application-data.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { ApplicationBankDataComponent } from './application-details/application-bank-data/application-bank-data.component';
import { ApplicationLicensesComponent } from './application-licenses/application-licenses.component';



@NgModule({
  declarations: [ApplicationListComponent, ApplicationDetailsComponent, ApplicationDataComponent, ApplicationBankDataComponent, ApplicationCreateComponent, ApplicationDeleteComponent, ApplicationUpdateComponent, ApplicationLicensesComponent],
  imports: [
    CommonModule,
    ApplicationRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [ApplicationListComponent, ApplicationDetailsComponent, ApplicationDataComponent, ApplicationBankDataComponent, ApplicationCreateComponent, ApplicationDeleteComponent, ApplicationUpdateComponent, ApplicationLicensesComponent],
})
export class ApplicationModule { }
