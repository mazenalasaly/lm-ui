import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { RepositoryService } from '../../shared/repository.service';
import { License } from 'src/app/_interface/license';
import { Application } from 'src/app/_interface/application';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDialogComponent } from '../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { SuccessDialogComponent } from '../../shared/dialogs/success-dialog/success-dialog.component';

@Component({
  selector: 'app-application-licenses',
  templateUrl: './application-licenses.component.html',
  styleUrls: ['./application-licenses.component.css']
})
export class ApplicationLicensesComponent implements OnInit {

  public displayedColumns = ['licenseCode', 'dateCreated', 'dateExpired', 'createdBy', 'isActivated'];
  public dataSource = new MatTableDataSource<License>();
  private confirmationDialogConfig;
  private successDialogConfig;
  private errorDialogConfig;
  private licenses : License[];
  public errorMessage: string = '';
  private licenseFile: Blob;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
 
  constructor(private repository: RepositoryService, private repoService: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router, private dialog: MatDialog, private activeRoute: ActivatedRoute) { }
 
  ngOnInit() {
    this.getAllApplicationLicenses();
    this.confirmationDialogConfig = {
        data: {
          title: 'Confirm Delete License',
          message: 'You are about to delete a license, Are you sure? '
        }
    }
    this.successDialogConfig = {
      data: { 
        title: 'Deleted Successfully',
        message: 'It is deleted successfully' },
  }

  this.errorDialogConfig = {
    data: {
      title: "Error Message",
      message: " "
    }
  }

  }
 
  public getAllApplicationLicenses() {
    let applicationId: string = this.activeRoute.snapshot.params['id'];
      
    let applicationLicensesByIdUrl: string = `api/application/${applicationId}/licenses`;
    this.repoService.getData(applicationLicensesByIdUrl)
    .subscribe(res=> {
      this.licenses = res as License[];
      this.dataSource.data = this.licenses;
    },
    (error)=> {
      this.errorHandler.handleError(error);
    })
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public customSort (event)  {
    console.log(event);
  }

  public doFilter (value: string)  {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  // public redirectToDetails (id: number) {
  //   this.router.navigate(['license','details',id]);   
  // }
 
  // public redirectToUpdate (id: number)  {
  //   this.router.navigate(['license','update',id]);     
  // }
 
  // public redirectToDelete (id: number)  {
  //   this.router.navigate(['license','delete',id]);    
  // }

  // public redirectToDownload (id: number)  {
  //   // this.router.navigate(['license','download',id]);    
  //   console.log("Downloading ...");
  // }
 
  // deleteLicense(id: Number) {
  //   const confirmDialog = this.dialog.open(ConfirmDialogComponent, this.confirmationDialogConfig);
    
  //   confirmDialog.afterClosed().subscribe(result => {
  //     if (result === true) {
  //      this.executeDeleteLicense(id);
  //     }
  //   });
  // }

  // public executeDeleteLicense (id: Number) {
  //   const deleteUrl: string = `api/license/delete/${id}`;
   
  //   this.repository.delete(deleteUrl)
  //   .subscribe(res=> {
  //       let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
  //       dialogRef.afterClosed()
  //          .subscribe(result=> {
  //           this.refreshPage();
  //         });
  //   },
  //     (error)=> {
  //       this.errorHandler.handleError(error);
  //       this.errorMessage = this.errorHandler.errorMessage;
  //     })
  // }

  // refreshPage() {
  //   window.location.reload();
  //  }

  //  public downloadLicense (id: Number) {
  //   const downloadURL: string = `api/license/download/${id}`;
   
  //   this.repository.getData(downloadURL)
  //   .subscribe(res=> {
  //     this.licenseFile = res as Blob;
  //   },
  //   (error)=> {
  //     this.errorHandler.errorDialogConfig = { ...this.errorDialogConfig };
  //     this.errorHandler.handleError(error);
  //   })
  // }


     
  public redirectToApplicationList () {
    this.router.navigate(['application','applications']);    
  }

  

}
