import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Application } from 'src/app/_interface/application';
 
@Component({
  selector: 'app-application-data',
  templateUrl: './application-data.component.html',
  styleUrls: ['./application-data.component.css']
})
export class ApplicationDataComponent implements OnInit {
  @Input() public application: Application;
  public selectOptions = [{name:'Show', value: 'show'}, {name: `Don't Show`, value: ''}];
  @Output() selectEmitt = new EventEmitter();
 
  constructor() { }
 
  ngOnInit() {
  }
 
  public onChange(event) {
    this.selectEmitt.emit(event.value);
  }
 
}
