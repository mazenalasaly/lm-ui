import { Component, OnInit, Input } from '@angular/core';
import { Bank } from 'src/app/_interface/bank';

 
@Component({
  selector: 'app-application-bank-data',
  templateUrl: './application-bank-data.component.html',
  styleUrls: ['./application-bank-data.component.css']
})
export class ApplicationBankDataComponent implements OnInit {
  @Input() public banks: Bank[];
  constructor() { }
 
  ngOnInit() {
  }
 
}