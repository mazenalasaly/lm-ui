import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RepositoryService } from '../../shared/repository.service';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Application } from 'src/app/_interface/application';
import { Bank } from 'src/app/_interface/bank';

 
@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})
export class ApplicationDetailsComponent implements OnInit {
  public application: Application;
  public showBanks;
  private banks: Bank[];
 
  constructor(private repository: RepositoryService, private router: Router, 
    private activeRoute: ActivatedRoute, private errorHandler: ErrorHandlerService) { }
 
  ngOnInit() {
    this.getApplicationDetails();
  }
 
  private getApplicationDetails(){
    let id: string = this.activeRoute.snapshot.params['id'];
    let apiUrl: string = `api/application/${id}/banks`;
 
    this.repository.getData(apiUrl)
    .subscribe(res=> {
      this.application = res as Application;
    },
    (error)=>{
      this.errorHandler.handleError(error);
    })
  }
}