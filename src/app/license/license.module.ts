import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { LicenseRoutingModule } from './license-routing/license-routing.module';
import { LicenseCreateComponent } from './license-create/license-create.component';
import { LicenseListComponent } from './license-list/license-list.component';
import { LicenseUpdateComponent } from './license-update/license-update.component';
import { LicenseDeleteComponent } from './license-delete/license-delete.component';
import { RenewalLisencesComponent } from './renewal-lisences/renewal-lisences.component';



@NgModule({
  declarations: [LicenseListComponent,  LicenseCreateComponent, LicenseUpdateComponent, LicenseDeleteComponent, RenewalLisencesComponent],
  imports: [
    CommonModule,
    LicenseRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports:[LicenseListComponent,  LicenseCreateComponent, LicenseUpdateComponent, LicenseDeleteComponent, RenewalLisencesComponent],
  providers: [
    DatePipe
  ]
})
export class LicenseModule { }
