import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { License } from 'src/app/_interface/license';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Bank } from 'src/app/_interface/bank';
import { Application } from 'src/app/_interface/application';

 
@Component({
  selector: 'app-license-update',
  templateUrl: './license-update.component.html',
  styleUrls: ['./license-update.component.css']
})
export class LicenseUpdateComponent implements OnInit {
 
  public errorMessage: string = '';
  public license: License;
  public licenseForm: FormGroup;
  private successDialogConfig;
  banks: Bank[];
  applications: Application[];
  bank: Bank;
  application: Application;
  // isActivated: boolean;
 
  constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
    private activeRoute: ActivatedRoute, private datePipe: DatePipe,  private dialog: MatDialog) { }
 
    ngOnInit() {
      // this.getAllBanks();
      // this.getAllApplications();
      this.getLicenseById();
      

      this.licenseForm = new FormGroup({
        // bank: new FormControl({value:'', disabled: true}, [Validators.required]),
        // application: new FormControl({value:'', disabled: true}, [Validators.required]),
        licenseCode: new FormControl({value:'', disabled: true},  [Validators.required]),
        dateCreated: new FormControl({value:'', disabled: true}, [Validators.required]),
        dateExpired: new FormControl({value:'', disabled: true}, [Validators.required]),
        isActivated: new FormControl('', [Validators.required]),
      });

      this.successDialogConfig = {
        data: {
          title: "Updated Successfully",
          message: "The selected license has been successfully updated"
        }
      }
  
    }
     
    ngAfterViewInit() {
     
    }

    public validateControl(controlName: string) {
      if (this.licenseForm.controls[controlName].invalid && this.licenseForm.controls[controlName].touched)
        return true;
     
      return false;
    }
     
    public hasError(controlName: string, errorName: string) {
      if (this.licenseForm.controls[controlName].hasError(errorName))
        return true;
     
      return false;
    }
     
    public executeDateCreatedPicker(event) {
      this.licenseForm.patchValue({ 'dateCreated': event });
    }

    public executeDateExpiredPicker(event) {
      this.licenseForm.patchValue({ 'dateExpired': event });
    }
     
    public redirectToLicenseList() {
      this.router.navigate(['license','licenses']);    
    }

    public updateLicense(licenseFormValue) {
      if (this.licenseForm.valid) {
        this.executeLicenseUpdate(licenseFormValue);
      }
    }
     
    private executeLicenseUpdate(licenseFormValue) {
      // let selectedBank: Bank;
      // this.banks.forEach(bankFilteredFromList => {
      //   if (bankFilteredFromList.id == licenseFormValue.bank) {
      //     selectedBank = bankFilteredFromList;
      //   }
      // })
      // let selectedApplication: Application;
      // this.applications.forEach(applicationFilteredFromList => {
      //   if (applicationFilteredFromList.id == licenseFormValue.application) {
      //     selectedApplication = applicationFilteredFromList;
      //   }
      // })
      
      // this.license.bank= selectedBank,
      // this.license.application= selectedApplication,
      this.license.isActivated = licenseFormValue.isActivated;
     
      const apiUrl = `api/license/update/${this.license.id}`;
      this.repository.update(apiUrl, this.license)
        .subscribe(rres=> {
          let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
   
          //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
          dialogRef.afterClosed()
            .subscribe(result=> {
              this.router.navigate(['license','licenses']); 
            });
        },
        (error=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
      )
    }

    // private getAllBanks() {
    //   this.repository.getData('api/bank/banks')
    //     .subscribe(res => {
    //       this.banks = res as Bank[];
    //     },
    //       (error) => {
    //         this.errorHandler.handleError(error);
    //       })
    // }
  
    // private getAllApplications() {
    //   this.repository.getData('api/application/applications')
    //     .subscribe(res => {
    //       this.applications = res as Application[];
    //     },
    //       (error) => {
    //         this.errorHandler.handleError(error);
    //       })
    // }

    private getLicenseById() {
      let licenseId: string = this.activeRoute.snapshot.params['id'];
        
      let licenseByIdUrl: string = `api/license/details/${licenseId}`;
     
      this.repository.getData(licenseByIdUrl)
        .subscribe(res=> {
          this.license = res as License;
          this.licenseForm.patchValue(this.license);

        },
        (error)=> {
          this.errorHandler.handleError(error);
          this.errorMessage = this.errorHandler.errorMessage;
        })
    }
  
}
