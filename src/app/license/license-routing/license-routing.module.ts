import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LicenseCreateComponent } from '../license-create/license-create.component';
import { LicenseUpdateComponent } from '../license-update/license-update.component';
import { LicenseDeleteComponent } from '../license-delete/license-delete.component';
import { LicenseListComponent } from '../license-list/license-list.component';
import { RenewalLisencesComponent } from '../renewal-lisences/renewal-lisences.component';


const routes: Routes = [
  { path: 'licenses', component: LicenseListComponent },
  { path: 'create', component: LicenseCreateComponent },
  { path: 'licenses/renwalLicenses', component: RenewalLisencesComponent },
  { path: 'update/:id', component: LicenseUpdateComponent },
  { path: 'delete/:id', component: LicenseDeleteComponent }
];
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class LicenseRoutingModule { }
