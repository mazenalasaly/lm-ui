import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator'
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { Router } from '@angular/router';
import { License } from 'src/app/_interface/license';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/dialogs/confirm-dialog/confirm-dialog.component';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
 
@Component({
  selector: 'app-license-list',
  templateUrl: './license-list.component.html',
  styleUrls: ['./license-list.component.css']
})
export class LicenseListComponent implements OnInit, AfterViewInit {
 
  public displayedColumns = ['licenseCode', 'dateCreated', 'dateExpired', 'createdBy', 'isActivated', 'listRenewalLicenses', 'download',  'update', 'delete'];
  public dataSource = new MatTableDataSource<License>();
  private confirmationDialogConfig;
  private successDialogConfig;
  private errorDialogConfig;
  private licenses : License[];
  public errorMessage: string = '';
  private licenseFile: Blob;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
 
  constructor(private repository: RepositoryService, private repoService: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router, private dialog: MatDialog) { }
 
  ngOnInit() {
    this.getAllLicenses();
    this.confirmationDialogConfig = {
        data: {
          title: 'Confirm Delete License',
          message: 'You are about to delete a license, Are you sure? '
        }
    }
    this.successDialogConfig = {
      data: { 
        title: 'Deleted Successfully',
        message: 'It is deleted successfully' },
  }

  this.errorDialogConfig = {
    data: {
      title: "Error Message",
      message: " "
    }
  }

  }
 
  public getAllLicenses() {
    this.repoService.getData('api/license/licenses')
    .subscribe(res=> {
      this.licenses = res as License[];
      this.dataSource.data = this.licenses;
    },
    (error)=> {
      this.errorHandler.handleError(error);
    })
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public customSort (event)  {
    console.log(event);
  }

  public doFilter (value: string)  {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
 
  public redirectToDetails (id: number) {
    this.router.navigate(['license','details',id]);   
  }
 
  public redirectToUpdate (id: number)  {
    this.router.navigate(['license','update',id]);     
  }
 
  public redirectToDelete (id: number)  {
    this.router.navigate(['license','delete',id]);    
  }

  public redirectToDownload (id: number)  {
    // this.router.navigate(['license','download',id]);    
    console.log("Downloading ...");
  }

  

  public redirectTolistRenewalLicenses ()  {
    this.router.navigate(['license','licenses','renwalLicenses']);    
  }

 
  deleteLicense(id: Number) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, this.confirmationDialogConfig);
    
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
       this.executeDeleteLicense(id);
      }
    });
  }

  public executeDeleteLicense (id: Number) {
    const deleteUrl: string = `api/license/delete/${id}`;
   
    this.repository.delete(deleteUrl)
    .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
        dialogRef.afterClosed()
           .subscribe(result=> {
            this.refreshPage();
          });
    },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }

  refreshPage() {
    window.location.reload();
   }

   public downloadLicense (id: Number) {
    const downloadURL: string = `api/license/download/${id}`;
   
    this.repository.getData(downloadURL)
    .subscribe(res=> {
      this.licenseFile = res as Blob;
    },
    (error)=> {
      this.errorHandler.errorDialogConfig = { ...this.errorDialogConfig };
      this.errorHandler.handleError(error);
    })
  }

 
}
