import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorHandlerService } from 'src/app/shared/error-handler.service';
import { RepositoryService } from 'src/app/shared/repository.service';
import { License } from 'src/app/_interface/license';
import { SuccessDialogComponent } from '../../shared/dialogs/success-dialog/success-dialog.component';
 
@Component({
  selector: 'app-license-delete',
  templateUrl: './license-delete.component.html',
  styleUrls: ['./license-delete.component.css']
})
export class LicenseDeleteComponent implements OnInit {
  public errorMessage: string = '';
  public license: License;
  private dialogSuccessConfig;
 
constructor(private repository: RepositoryService, private errorHandler: ErrorHandlerService, private router: Router,
  private activeRoute: ActivatedRoute, private dialog: MatDialog) { }
 
  ngOnInit() {
    this.getLicenseById();
    
    this.dialogSuccessConfig = {
      data: { 
        title: 'Deleted Successfully',
        message: 'It is deleted successfully' },
  }
}
   
  private getLicenseById () {
    const licenseId: string = this.activeRoute.snapshot.params['id'];
    const licenseByIdUrl: string = `api/license/details/${licenseId}`;
   
    this.repository.getData(licenseByIdUrl)
      .subscribe(res=> {
        this.license = res as License;
      },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }
   
     
  public redirectToLicenseList () {
    this.router.navigate(['license','licenses']);    
  }

  public deleteLicense () {
    const deleteUrl: string = `api/license/delete/${this.license.id}`;
   
    this.repository.delete(deleteUrl)
    .subscribe(res=> {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.dialogSuccessConfig);
        dialogRef.afterClosed()
           .subscribe(result=> {
            this.redirectToLicenseList();
          });
    },
      (error)=> {
        this.errorHandler.handleError(error);
        this.errorMessage = this.errorHandler.errorMessage;
      })
  }

  
}

