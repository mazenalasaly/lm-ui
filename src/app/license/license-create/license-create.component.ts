import { RepositoryService } from '../../shared/repository.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from 'src/app/shared/dialogs/success-dialog/success-dialog.component';
import { ErrorHandlerService } from '../../shared/error-handler.service';
import { LicenseForCreation } from 'src/app/_interface/licenseForCreation';
import { Bank } from 'src/app/_interface/bank';
import { Router } from '@angular/router';
import { Application } from 'src/app/_interface/application';
import { BankModule } from 'src/app/bank/bank.module';
import { ConfirmDialogComponent } from 'src/app/shared/dialogs/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-license-create',
  templateUrl: './license-create.component.html',
  styleUrls: ['./license-create.component.css']
})
export class LicenseCreateComponent implements OnInit {
  public licenseForm: FormGroup;
  private successDialogConfig;
  private confirmationDialogConfig;
  private errorDialogConfig;  
  private generatedLicenseCode;
  private selectedApplication: Application;
  private selectedBank: Bank;
  private dateExpired: Date;
  banks: Bank[];
  applications: Application[];
  licensesCodes: string [];
  currentDate: Date = new Date();
  userId: Number = 1;


  constructor(private router: Router, private repository: RepositoryService, private dialog: MatDialog, private errorService: ErrorHandlerService) { }

  ngOnInit() {
    this.getAllBanks();
    this.getAllApplications();
    this.getLicenseCode();
    this.licenseForm = new FormGroup({
      bank: new FormControl('', [Validators.required]),
      application: new FormControl('', [Validators.required]),
      dateExpired: new FormControl('', [Validators.required]),
      dateCreated: new FormControl({ value: this.currentDate, disabled: true })
    });

    this.successDialogConfig = {
      data: {
        title: "Created Successfully",
        message: "The required license has been successfully created"
      }
    }

    this.confirmationDialogConfig = {
      data: {
        title: "Are You Sure",
        message: "You are about to create a license already exist .."
      }
    }

    this.errorDialogConfig = {
      data: {
        title: "Error Message",
        message: " "
      }
    }

  }

  public hasError(controlName: string, errorName: string) {
    return this.licenseForm.controls[controlName].hasError(errorName);
  }

  public onCancel() {
    this.router.navigate(['license', 'licenses']);
  }

  public createLicense(licenseFormValue) {
    if (this.licenseForm.valid) {
      
    this.banks.forEach(bankFilteredFromList => {
      if (bankFilteredFromList.id == licenseFormValue.bank) {
        this.selectedBank = bankFilteredFromList;
      }
    })
    
    this.applications.forEach(applicationFilteredFromList => {
      if (applicationFilteredFromList.id == licenseFormValue.application) {
        this.selectedApplication = applicationFilteredFromList;
      }
    })
    this.dateExpired= licenseFormValue.dateExpired;

   this.generateLicenseCode();
   this.CheckIfTheBankIsLicensedForThisApplication();
    }
  }

  private executeLicenseCreation() {
    
    let license: LicenseForCreation = {
      // licenseName: selectedApplication.applicationName,
      bank: this.selectedBank,
      application: this.selectedApplication,
      licenseCode: this.generatedLicenseCode,
      dateCreated: this.currentDate,
      createdBy: 1,
      isActivated: true,
      dateExpired: this.dateExpired,
      licenseFile: null,
      
    }

    let apiUrl = 'api/license/create';
    this.repository.create(apiUrl, license)
      .subscribe(res => {
        let dialogRef = this.dialog.open(SuccessDialogComponent, this.successDialogConfig);
        dialogRef.afterClosed()
          .subscribe(result => {
            this.router.navigate(['license', 'licenses']);
          });
      },
        (error => {
          this.errorService.errorDialogConfig = { ...this.errorDialogConfig };
          this.errorService.handleError(error);
        })
      )
  }

  public getAllBanks() {
    this.repository.getData('api/bank/banks')
      .subscribe(res => {
        this.banks = res as Bank[];
      },
        (error) => {
          this.errorService.handleError(error);
        })
  }

  public getAllApplications() {
    this.repository.getData('api/application/applications')
      .subscribe(res => {
        this.applications = res as Application[];
      },
        (error) => {
          this.errorService.handleError(error);
        })
  }

  public getLicenseCode() {
    this.repository.getData('api/license/licensesCodes')
      .subscribe(res => {
        this.licensesCodes = res as string[];
        console.log("this.licenseCode" + this.licensesCodes);
      },
        (error) => {
          this.errorService.handleError(error);
          console.log(error);
        })
  }

  public generateLicenseCode (){
    this.generatedLicenseCode = this.selectedBank.bankName.concat(this.selectedApplication.applicationName); 
  }

  public CheckIfTheBankIsLicensedForThisApplication (){
    if ((this.licensesCodes.includes(this.generatedLicenseCode))) {
      this.OpenConfirmationDialog();
    }
    else{
      this.executeLicenseCreation();
    }
  }

    public OpenConfirmationDialog(){

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, this.confirmationDialogConfig);
    
    confirmDialog.afterClosed().subscribe(result => {
      if(result) {
        this.executeLicenseCreation();
      }
    });
  }
  

}

